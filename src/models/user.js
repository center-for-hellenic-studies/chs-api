import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * Users in the CHS website
 */
const User = db.define('user', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	userType_id: {
		type: Sequelize.INTEGER,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},

	name_first: {
		type: Sequelize.CHAR(45),
	},
	name_last: {
		type: Sequelize.CHAR(45),
	},
	phone: {
		type: Sequelize.CHAR(45),
	},
	bio: {
		type: Sequelize.TEXT,
	},
	email: {
		type: Sequelize.CHAR(45),
	},
	password: {
		type: Sequelize.CHAR(45),
	},
	status: {
		type: Sequelize.CHAR(45),
	},
	name_middle: {
		type: Sequelize.CHAR(45),
	},
	title: {
		type: Sequelize.CHAR(45),
	},
	version: {
		type: Sequelize.BIGINT,
	},
	account_expired: {
		type: Sequelize.TINYINT,
	},
	account_locked: {
		type: Sequelize.TINYINT,
	},
	enabled: {
		type: Sequelize.TINYINT,
	},
	username: {
		type: Sequelize.STRING,
	},
	reset_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
});


export default User;
