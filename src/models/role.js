import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An role native to Grails/Groovy
 */
const Role = db.define('role', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	name: {
		type: Sequelize.CHAR(50),
	},
	version: {
		type: Sequelize.BIGINT,
	},
}, {
	timestamps: false,
});


export default Role;
