import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * Global site settings
 */
const Site = db.define('site', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	site_key: {
		type: Sequelize.CHAR(45),
	},
	site_value: {
		type: Sequelize.TEXT,
	},
	value: {
		type: Sequelize.TEXT,
	},
}, {
	timestamps: false,
});


export default Site;
