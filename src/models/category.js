import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An category used to categorize articles
 */
const Category = db.define('category', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	title: {
		type: Sequelize.STRING,
	},
	descrip: {
		type: Sequelize.STRING,
	},
	version: {
		type: Sequelize.BIGINT,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},
	date_created_id: {
		type: Sequelize.INTEGER,
	},
	date_removed_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
});


export default Category;
