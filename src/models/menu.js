import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An menu published on the CHS website, used in header
 */
const Menu = db.define('menu', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	name: {
		type: Sequelize.STRING,
	},
	status: {
		type: Sequelize.STRING,
	},
	version: {
		type: Sequelize.BIGINT,
	},
	masterMenu_id: {
		type: Sequelize.INTEGER,
	},
	menu_type: {
		type: Sequelize.STRING,
	},
	link_id: {
		type: Sequelize.INTEGER,
	},
	m_order: {
		type: Sequelize.INTEGER,
	},
	collection_id: {
		type: Sequelize.INTEGER,
	},
	article_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
});


export default Menu;
