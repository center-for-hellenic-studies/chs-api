import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An article published on the CHS website, similar to Wordpress Posts, used for
 * publishing all manner of content
 */
const Article = db.define('article', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	item_id: {
		type: Sequelize.INTEGER,
	},
	menu_id: {
		type: Sequelize.INTEGER,
	},
	page_id: {
		type: Sequelize.INTEGER,
	},
	sector_id: {
		type: Sequelize.INTEGER,
	},
	author_id: {
		type: Sequelize.INTEGER,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},
	title: {
		type: Sequelize.STRING,
	},
	sub_title: {
		type: Sequelize.STRING,
	},
	audit_id: {
		type: Sequelize.INTEGER,
	},
	article_id: {
		type: Sequelize.INTEGER,
	},
	versionNum: {
		type: Sequelize.INTEGER,
	},
	isCurrent: {
		type: Sequelize.TINYINT,
	},
	date_publish: {
		type: Sequelize.DATE,
	},
	date_expire: {
		type: Sequelize.DATE,
	},
	summary: {
		type: Sequelize.TEXT,
	},
	articleContent: {
		type: Sequelize.TEXT,
	},
	version_id: {
		type: Sequelize.INTEGER,
	},
	featuredOn: {
		type: Sequelize.DATE,
	},
	media_id: {
		type: Sequelize.INTEGER,
	},
	version: {
		type: Sequelize.BIGINT,
	},
	date_expire_id: {
		type: Sequelize.INTEGER,
	},
	date_modified_id: {
		type: Sequelize.INTEGER,
	},
	date_publish_id: {
		type: Sequelize.INTEGER,
	},
	date_removed_id: {
		type: Sequelize.INTEGER,
	},
	thumbnail_id: {
		type: Sequelize.INTEGER,
	},
	date_created_id: {
		type: Sequelize.INTEGER,
	},
	optional_content: {
		type: Sequelize.TEXT,
	},
	display_type: {
		type: Sequelize.STRING,
	},
	topic_id: {
		type: Sequelize.INTEGER,
	},
	language_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
	tableName: 'article',
});


Article.associate = ({ models }) => {
  // Does Sequelize handle this correctly with mutations?
	// Article.hasMany(models.item);
	// Article.hasMany(models.menu);
	// Article.hasMany(models.page);
	// Article.hasMany(models.sector);
	// Article.hasMany(models.author);
};

export default Article;
