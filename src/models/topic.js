import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An topic associated with a article, author, page, or article, etc.
 */
const Topic = db.define('topic', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	category_id: {
		type: Sequelize.INTEGER,
	},
	name: {
		type: Sequelize.STRING,
	},
	descrip: {
		type: Sequelize.TEXT,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},
	status: {
		type: Sequelize.CHAR(45),
	},
	version: {
		type: Sequelize.BIGINT,
	},
	date_created_id: {
		type: Sequelize.INTEGER,
	},
	date_modified_id: {
		type: Sequelize.INTEGER,
	},
	date_removed_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
});


export default Topic;
