import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An collection of articles, represented with a menu item
 */
const Collection = db.define('collection', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},
	name: {
		type: Sequelize.STRING,
	},
	descrip: {
		type: Sequelize.STRING,
	},
	status: {
		type: Sequelize.CHAR(45),
	},
	version: {
		type: Sequelize.BIGINT,
	},
	menu_id: {
		type: Sequelize.INTEGER,
	},
	page_id: {
		type: Sequelize.INTEGER,
	},
	sector_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
});


export default Collection;
