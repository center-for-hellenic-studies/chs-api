import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * All media published on CHS website
 */
const Media = db.define('media', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	item_id: {
		type: Sequelize.INTEGER,
	},
	text_id: {
		type: Sequelize.INTEGER,
	},
	item_collection_id: {
		type: Sequelize.INTEGER,
	},
	type: {
		type: Sequelize.CHAR,
	},
	title: {
		type: Sequelize.CHAR(120),
	},
	summary: {
		type: Sequelize.TEXT,
	},
	fileName: {
		type: Sequelize.CHAR,
	},
	descrip: {
		type: Sequelize.TEXT,
	},
	status: {
		type: Sequelize.CHAR(45),
	},
	version: {
		type: Sequelize.BIGINT,
	},
	article_id: {
		type: Sequelize.INTEGER,
	},
}, {
	timestamps: false,
});


export default Media;
