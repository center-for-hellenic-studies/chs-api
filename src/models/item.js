import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * An item on the CHS website
 */
const Item = db.define('item', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	collection_id: {
		type: Sequelize.INTEGER,
	},
	iorder: {
		type: Sequelize.INTEGER,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},
	version: {
		type: Sequelize.BIGINT,
	},
}, {
	timestamps: false,
});


export default Item;
