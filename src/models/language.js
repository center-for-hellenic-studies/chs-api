import Sequelize from 'sequelize';
import SequelizeSlugify from 'sequelize-slugify';
import db from '../db';

/**
 * A language of a resource on the CHS website
 */
const Language = db.define('language', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
	},
	text_id: {
		type: Sequelize.INTEGER,
	},
	date_created: {
		type: Sequelize.DATE,
	},
	created_by: {
		type: Sequelize.INTEGER,
	},
	date_modified: {
		type: Sequelize.DATE,
	},
	modified_by: {
		type: Sequelize.INTEGER,
	},
	date_removed: {
		type: Sequelize.DATE,
	},
	removed_by: {
		type: Sequelize.INTEGER,
	},
	language: {
		type: Sequelize.CHAR(100),
	},
	article_id: {
		type: Sequelize.INTEGER,
	},
	version: {
		type: Sequelize.BIGINT,
	},
}, {
	timestamps: false,
});


export default Language;
