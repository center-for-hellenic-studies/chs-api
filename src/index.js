// main
import 'babel-polyfill';
import express from 'express';
import fs from 'fs';
import path from 'path';
import request from 'request';
import winston from 'winston';

// middleware
import bodyParser from 'body-parser';
import session from 'express-session';
import logger from 'morgan';
import cookieParser from 'cookie-parser';

// dotenv
import dotenvSetup from './dotenv';

// mongoDB
import db, { dbSetup } from './db';

// authentication
import authSetup from './authentication';

// cors
import corsSetup from './cors';

// graphQL
import setupGraphql from './graphql';

// S3
import s3Setup from './s3';

// Redis
import redisSetup from './redis';

// OAuth setup
import oauthSetup from './oauth';

// email
import EmailManager from './email';

// Routes
import authenticationRouter from './authentication/routes';

// environment variables setup
dotenvSetup();

const app = express();

dbSetup();

const redisClient = redisSetup();

app.set('port', (process.env.PORT || 3001));

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'));
}

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
	extended: false,
	limit: '50mb',
}));

// CORS setup
corsSetup(app);

// Authentication setup
authSetup(app, redisClient);

// GraphQl setup
setupGraphql(app);

// S3 setup
s3Setup(app);

// OAuth setup
oauthSetup(app);

// Routes
app.use('/auth', authenticationRouter);


function listen() {
	app.listen(app.get('port'), () => {
		console.info(`Application listening on port ${app.get('port')}`);
	});
}

// Connect to db and then start express listen
db.authenticate()
	.then(async () => {
		await db.sync();

		listen();
	})
	.catch((e) => {
		winston.error(`Could not authenticate to database ${process.env.DB_NAME}`);
		winston.error(e);
	});
