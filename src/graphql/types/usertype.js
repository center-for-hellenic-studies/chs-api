import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import UserType from '../../models/usertype';

/**
 * UserType model type
 * @type {GraphQLObjectType}
 */
const UserTypeType = new GraphQLObjectType({
	name: 'UserType',
	description: 'An article on the CHS website',
	fields: attributeFields(UserType),
});

export default UserTypeType;
