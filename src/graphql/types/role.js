import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Role from '../../models/role';

/**
 * Role model type
 * @type {GraphQLObjectType}
 */
const RoleType = new GraphQLObjectType({
	name: 'Role',
	description: 'An article on the CHS website',
	fields: attributeFields(Role),
});

export default RoleType;
