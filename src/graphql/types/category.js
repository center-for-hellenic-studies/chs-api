import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Category from '../../models/category';

/**
 * Category model type
 * @type {GraphQLObjectType}
 */
const CategoryType = new GraphQLObjectType({
	name: 'Category',
	description: 'An article on the CHS website',
	fields: attributeFields(Category),
});

export default CategoryType;
