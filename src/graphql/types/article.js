import _ from 'underscore';
import {
	GraphQLObjectType, GraphQLInputObjectType, GraphQLNonNull, GraphQLList,
	GraphQLSchema, GraphQLInt, GraphQLString,
} from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Article from '../../models/article';

/**
 * Article model type
 * @type {GraphQLObjectType}
 */
const ArticleType = new GraphQLObjectType({
	name: 'Article',
	description: 'An article on the CHS website',
	fields: attributeFields(Article),
});

/**
 * Article input type
 * @type {GraphQLInputObjectType}
 */
const ArticleInputType = new GraphQLInputObjectType({
	name: 'ArticleInput',
	description: 'Input type for an article',
	fields: attributeFields(Article),
});

export { ArticleInputType };
export default ArticleType;
