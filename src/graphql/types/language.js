import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Language from '../../models/language';

/**
 * Language model type
 * @type {GraphQLObjectType}
 */
const LanguageType = new GraphQLObjectType({
	name: 'Language',
	description: 'An article on the CHS website',
	fields: attributeFields(Language),
});

export default LanguageType;
