import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Menu from '../../models/menu';

/**
 * Menu model type
 * @type {GraphQLObjectType}
 */
const MenuType = new GraphQLObjectType({
	name: 'Menu',
	description: 'An article on the CHS website',
	fields: attributeFields(Menu),
});

export default MenuType;
