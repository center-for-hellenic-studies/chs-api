import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Site from '../../models/site';

/**
 * Site model type
 * @type {GraphQLObjectType}
 */
const SiteType = new GraphQLObjectType({
	name: 'Site',
	description: 'An article on the CHS website',
	fields: attributeFields(Site),
});

export default SiteType;
