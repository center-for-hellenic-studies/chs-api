import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Item from '../../models/item';

/**
 * Item model type
 * @type {GraphQLObjectType}
 */
const ItemType = new GraphQLObjectType({
	name: 'Item',
	description: 'An article on the CHS website',
	fields: attributeFields(Item),
});

export default ItemType;
