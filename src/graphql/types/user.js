import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import User from '../../models/user';

/**
 * User model type
 * @type {GraphQLObjectType}
 */
const UserType = new GraphQLObjectType({
	name: 'User',
	description: 'An article on the CHS website',
	fields: attributeFields(User),
});

export default UserType;
