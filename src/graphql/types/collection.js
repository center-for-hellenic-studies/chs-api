import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Collection from '../../models/collection';

/**
 * Collection model type
 * @type {GraphQLObjectType}
 */
const CollectionType = new GraphQLObjectType({
	name: 'Collection',
	description: 'An article on the CHS website',
	fields: attributeFields(Collection),
});

export default CollectionType;
