import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Media from '../../models/media';

/**
 * Media model type
 * @type {GraphQLObjectType}
 */
const MediaType = new GraphQLObjectType({
	name: 'Media',
	description: 'An article on the CHS website',
	fields: attributeFields(Media),
});

export default MediaType;
