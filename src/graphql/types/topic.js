import _ from 'underscore';
import { GraphQLObjectType, GraphQLNonNull, GraphQLList, GraphQLSchema, GraphQLInt, GraphQLString } from 'graphql';
import { attributeFields } from 'graphql-sequelize';

import Topic from '../../models/topic';

/**
 * Topic model type
 * @type {GraphQLObjectType}
 */
const TopicType = new GraphQLObjectType({
	name: 'Topic',
	description: 'An article on the CHS website',
	fields: attributeFields(Topic),
});

export default TopicType;
