import { GraphQLObjectType } from 'graphql';

import articleMutationFields from './article';

/**
 * Root mutations
 * @type {GraphQLObjectType}
 */
const RootMutations = new GraphQLObjectType({
	name: 'RootMutationType',
	description: 'Root mutation object type',
	fields: {
		...articleMutationFields,
	},
});

export default RootMutations;
