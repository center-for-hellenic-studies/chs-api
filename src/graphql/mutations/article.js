import { GraphQLString, GraphQLNonNull, GraphQLID } from 'graphql';

// types
import ArticleType, { ArticleInputType } from '../types/article';
import RemoveType from '../types/remove';

// Logic
import ArticleService from '../logic/article';


const articleMutationFields = {
	articleCreate: {
		type: ArticleType,
		description: 'Create a new article',
		args: {
			article: {
				type: new GraphQLNonNull(ArticleInputType)
			},
		},
		async resolve(_, { article }, { token }) {
			const articleService = new ArticleService(token);
			return await articleService.create(article);
		}
	},
	articleUpdate: {
		type: ArticleType,
		description: 'Update article',
		args: {
			article: {
				type: new GraphQLNonNull(ArticleInputType),
			},
		},
		async resolve(_, { article }, { token }) {
			const articleService = new ArticleService(token);
			return await articleService.update(article);
		}
	},

	articleSave: {
		type: ArticleType,
		description: 'Save an article as a user is editing',
		args: {
			article: {
				type: new GraphQLNonNull(ArticleInputType)
			},
		},
		async resolve(_, { article }, { token }) {
			const articleService = new ArticleService(token);
			return await articleService.save(article);
		}
	},

	articleRemove: {
		type: RemoveType,
		description: 'Remove article',
		args: {
			id: {
				type: new GraphQLNonNull(GraphQLString),
			},
		},
		async resolve (_, { id, }, { token }) {
			const articleService = new ArticleService(token);
			return await articleService.remove(id);
		}
	}
};

export default articleMutationFields;
