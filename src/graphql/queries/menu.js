import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import MenuType from '../types/menu';

// Logic
import MenuService from '../logic/menu';


const menuQueryFields = {
	menu: {
		type: MenuType,
		description: 'Get menu document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const menuService = new MenuService(token);
			return menuService.getMenu({ id, slug, });
		}
	},
	menus: {
		type: new GraphQLList(MenuType),
		description: 'Get list of menus',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const menuService = new MenuService(token);
			return menuService.getMenus({ limit, offset, textsearch });
		}
	},
};

export default menuQueryFields;
