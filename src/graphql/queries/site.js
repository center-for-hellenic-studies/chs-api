import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import SiteType from '../types/site';

// Logic
import SiteService from '../logic/site';


const siteQueryFields = {
	site: {
		type: SiteType,
		description: 'Get site document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const siteService = new SiteService(token);
			return siteService.getSite({ id, slug, });
		}
	},
	sites: {
		type: new GraphQLList(SiteType),
		description: 'Get list of sites',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const siteService = new SiteService(token);
			return siteService.getSites({ limit, offset, textsearch });
		}
	},
};

export default siteQueryFields;
