import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import LanguageType from '../types/language';

// Logic
import LanguageService from '../logic/language';


const languageQueryFields = {
	language: {
		type: LanguageType,
		description: 'Get language document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const languageService = new LanguageService(token);
			return languageService.getLanguage({ id, slug, });
		}
	},
	languages: {
		type: new GraphQLList(LanguageType),
		description: 'Get list of languages',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const languageService = new LanguageService(token);
			return languageService.getLanguages({ limit, offset, textsearch });
		}
	},
};

export default languageQueryFields;
