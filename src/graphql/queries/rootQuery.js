import { GraphQLObjectType } from 'graphql';

import articleQueryFields from './article';
import categoryQueryFields from './category';
import collectionQueryFields from './collection';
import itemQueryFields from './item';
import languageQueryFields from './language';
import mediaQueryFields from './media';
import menuQueryFields from './menu';
import roleQueryFields from './role';
import siteQueryFields from './site';
import topicQueryFields from './topic';
import userQueryFields from './user';
import usertypeQueryFields from './usertype';

/**
 * Root Queries
 * @type {GraphQLObjectType}
 */
const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	description: 'Root query object type',
	fields: {
		...articleQueryFields,
		...categoryQueryFields,
		...collectionQueryFields,
		...itemQueryFields,
		...languageQueryFields,
		...mediaQueryFields,
		...menuQueryFields,
		...roleQueryFields,
		...siteQueryFields,
		...topicQueryFields,
		...userQueryFields,
		...usertypeQueryFields,
	},
});

export default RootQuery;
