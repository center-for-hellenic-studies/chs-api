import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import CollectionType from '../types/collection';

// Logic
import CollectionService from '../logic/collection';


const collectionQueryFields = {
	collection: {
		type: CollectionType,
		description: 'Get collection document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const collectionService = new CollectionService(token);
			return collectionService.getCollection({ id, slug, });
		}
	},
	collections: {
		type: new GraphQLList(CollectionType),
		description: 'Get list of collections',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const collectionService = new CollectionService(token);
			return collectionService.getCollections({ limit, offset, textsearch });
		}
	},
};

export default collectionQueryFields;
