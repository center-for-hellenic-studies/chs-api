import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import RoleType from '../types/role';

// Logic
import RoleService from '../logic/role';


const roleQueryFields = {
	role: {
		type: RoleType,
		description: 'Get role document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const roleService = new RoleService(token);
			return roleService.getRole({ id, slug, });
		}
	},
	roles: {
		type: new GraphQLList(RoleType),
		description: 'Get list of roles',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const roleService = new RoleService(token);
			return roleService.getRoles({ limit, offset, textsearch });
		}
	},
};

export default roleQueryFields;
