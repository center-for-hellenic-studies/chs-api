import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import TopicType from '../types/topic';

// Logic
import TopicService from '../logic/topic';


const topicQueryFields = {
	topic: {
		type: TopicType,
		description: 'Get topic document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const topicService = new TopicService(token);
			return topicService.getTopic({ id, slug, });
		}
	},
	topics: {
		type: new GraphQLList(TopicType),
		description: 'Get list of topics',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const topicService = new TopicService(token);
			return topicService.getTopics({ limit, offset, textsearch });
		}
	},
};

export default topicQueryFields;
