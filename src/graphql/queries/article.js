import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import ArticleType from '../types/article';

// Logic
import ArticleService from '../logic/article';


const articleQueryFields = {
	article: {
		type: ArticleType,
		description: 'Get article document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const articleService = new ArticleService(token);
			return articleService.getArticle({ id, slug });
		}
	},
	articles: {
		type: new GraphQLList(ArticleType),
		description: 'Get list of articles',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const articleService = new ArticleService(token);
			return articleService.getArticles({ limit, offset, textsearch });
		}
	},
};

export default articleQueryFields;
