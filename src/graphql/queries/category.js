import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import CategoryType from '../types/category';

// Logic
import CategoryService from '../logic/category';


const categoryQueryFields = {
	category: {
		type: CategoryType,
		description: 'Get category document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const categoryService = new CategoryService(token);
			return categoryService.getCategory({ id, slug, });
		}
	},
	categories: {
		type: new GraphQLList(CategoryType),
		description: 'Get list of categories',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const categoryService = new CategoryService(token);
			return categoryService.getCategories({ limit, offset, textsearch });
		}
	},
};

export default categoryQueryFields;
