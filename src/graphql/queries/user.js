import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import UserType from '../types/user';

// Logic
import UserService from '../logic/user';


const userQueryFields = {
	user: {
		type: UserType,
		description: 'Get user document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const userService = new UserService(token);
			return userService.getUser({ id, slug, });
		}
	},
	users: {
		type: new GraphQLList(UserType),
		description: 'Get list of users',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const userService = new UserService(token);
			return userService.getUsers({ limit, offset, textsearch });
		}
	},
};

export default userQueryFields;
