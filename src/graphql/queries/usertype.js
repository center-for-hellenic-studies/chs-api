import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import UserTypeType from '../types/usertype';

// Logic
import UserTypeService from '../logic/usertype';


const usertypeQueryFields = {
	usertype: {
		type: UserTypeType,
		description: 'Get usertype document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const usertypeService = new UserTypeService(token);
			return usertypeService.getUserType({ id, slug, });
		}
	},
	usertypes: {
		type: new GraphQLList(UserTypeType),
		description: 'Get list of usertypes',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const usertypeService = new UserTypeService(token);
			return usertypeService.getUserTypes({ limit, offset, textsearch });
		}
	},
};

export default usertypeQueryFields;
