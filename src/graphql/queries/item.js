import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import ItemType from '../types/item';

// Logic
import ItemService from '../logic/item';


const itemQueryFields = {
	item: {
		type: ItemType,
		description: 'Get item document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const itemService = new ItemService(token);
			return itemService.getItem({ id, slug, });
		}
	},
	items: {
		type: new GraphQLList(ItemType),
		description: 'Get list of items',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const itemService = new ItemService(token);
			return itemService.getItems({ limit, offset, textsearch });
		}
	},
};

export default itemQueryFields;
