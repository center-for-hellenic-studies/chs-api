import { GraphQLInt, GraphQLString, GraphQLNonNull, GraphQLList } from 'graphql';

// types
import MediaType from '../types/media';

// Logic
import MediaService from '../logic/media';


const mediaQueryFields = {
	media: {
		type: MediaType,
		description: 'Get media document',
		args: {
			id: {
				type: GraphQLString,
			},
			slug: {
				type: GraphQLString,
			},
		},
		resolve(parent, { id, slug, }, { token }) {
			const mediaService = new MediaService(token);
			return mediaService.getMedia({ id, slug, });
		}
	},
	medias: {
		type: new GraphQLList(MediaType),
		description: 'Get list of medias',
		args: {
			textsearch: {
				type: GraphQLString,
			},
			limit: {
				type: GraphQLInt,
			},
			offset: {
				type: GraphQLInt,
			},
		},
		resolve(parent, { limit, offset, textsearch }, { token }) {
			const mediaService = new MediaService(token);
			return mediaService.getMedias({ limit, offset, textsearch });
		}
	},
};

export default mediaQueryFields;
