import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Collection from '../../models/collection';


/**
 * Logic-layer service for dealing with collections
 */

export default class CollectionService extends PermissionsService {
	/**
	 * Count collections
	 * @returns {number} count of collections
	 */
	count() {
		return Collection.count();
	}

	/**
	 * Get a list of collections
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of collections
	 */
	getCollections({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Collection.findAll(args);
	}

	/**
	 * Get collection
	 * @param {number} id - id of collection
	 * @param {string} slug - slug of collection
	 * @returns {Object} array of collections
	 */
	getCollection({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Collection.findOne({ where });
	}
}
