import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Item from '../../models/item';


/**
 * Logic-layer service for dealing with items
 */

export default class ItemService extends PermissionsService {
	/**
	 * Count items
	 * @returns {number} count of items
	 */
	count() {
		return Item.count();
	}

	/**
	 * Get a list of items
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of items
	 */
	getItems({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Item.findAll(args);
	}

	/**
	 * Get item
	 * @param {number} id - id of item
	 * @param {string} slug - slug of item
	 * @returns {Object} array of items
	 */
	getItem({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Item.findOne({ where });
	}
}
