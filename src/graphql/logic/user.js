import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import User from '../../models/user';


/**
 * Logic-layer service for dealing with users
 */

export default class UserService extends PermissionsService {
	/**
	 * Count users
	 * @returns {number} count of users
	 */
	count() {
		return User.count();
	}

	/**
	 * Get a list of users
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of users
	 */
	getUsers({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return User.findAll(args);
	}

	/**
	 * Get user
	 * @param {number} id - id of user
	 * @param {string} slug - slug of user
	 * @returns {Object} array of users
	 */
	getUser({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return User.findOne({ where });
	}
}
