import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Article from '../../models/article';


/**
 * Logic-layer service for dealing with articles
 */

export default class ArticleService extends PermissionsService {
	/**
	 * Count articles
	 * @returns {number} count of articles
	 */
	count() {
		return Article.count();
	}

	/**
	 * Get a list of articles
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of articles
	 */
	getArticles({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Article.findAll(args);
	}

	/**
	 * Get article
	 * @param {number} id - id of article
	 * @param {string} slug - slug of article
	 * @returns {Object} array of articles
	 */
	getArticle({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Article.findOne({ where });
	}
}
