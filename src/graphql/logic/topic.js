import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Topic from '../../models/topic';


/**
 * Logic-layer service for dealing with topics
 */

export default class TopicService extends PermissionsService {
	/**
	 * Count topics
	 * @returns {number} count of topics
	 */
	count() {
		return Topic.count();
	}

	/**
	 * Get a list of topics
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of topics
	 */
	getTopics({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Topic.findAll(args);
	}

	/**
	 * Get topic
	 * @param {number} id - id of topic
	 * @param {string} slug - slug of topic
	 * @returns {Object} array of topics
	 */
	getTopic({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Topic.findOne({ where });
	}
}
