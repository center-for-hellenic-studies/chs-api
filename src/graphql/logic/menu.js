import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Menu from '../../models/menu';


/**
 * Logic-layer service for dealing with menus
 */

export default class MenuService extends PermissionsService {
	/**
	 * Count menus
	 * @returns {number} count of menus
	 */
	count() {
		return Menu.count();
	}

	/**
	 * Get a list of menus
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of menus
	 */
	getMenus({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Menu.findAll(args);
	}

	/**
	 * Get menu
	 * @param {number} id - id of menu
	 * @param {string} slug - slug of menu
	 * @returns {Object} array of menus
	 */
	getMenu({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Menu.findOne({ where });
	}
}
