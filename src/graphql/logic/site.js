import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Site from '../../models/site';


/**
 * Logic-layer service for dealing with sites
 */

export default class SiteService extends PermissionsService {
	/**
	 * Count sites
	 * @returns {number} count of sites
	 */
	count() {
		return Site.count();
	}

	/**
	 * Get a list of sites
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of sites
	 */
	getSites({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Site.findAll(args);
	}

	/**
	 * Get site
	 * @param {number} id - id of site
	 * @param {string} slug - slug of site
	 * @returns {Object} array of sites
	 */
	getSite({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Site.findOne({ where });
	}
}
