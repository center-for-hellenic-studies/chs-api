import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Language from '../../models/language';


/**
 * Logic-layer service for dealing with languages
 */

export default class LanguageService extends PermissionsService {
	/**
	 * Count languages
	 * @returns {number} count of languages
	 */
	count() {
		return Language.count();
	}

	/**
	 * Get a list of languages
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of languages
	 */
	getLanguages({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Language.findAll(args);
	}

	/**
	 * Get language
	 * @param {number} id - id of language
	 * @param {string} slug - slug of language
	 * @returns {Object} array of languages
	 */
	getLanguage({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Language.findOne({ where });
	}
}
