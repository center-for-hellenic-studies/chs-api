import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Media from '../../models/media';


/**
 * Logic-layer service for dealing with medias
 */

export default class MediaService extends PermissionsService {
	/**
	 * Count medias
	 * @returns {number} count of medias
	 */
	count() {
		return Media.count();
	}

	/**
	 * Get a list of medias
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of medias
	 */
	getMedias({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Media.findAll(args);
	}

	/**
	 * Get media
	 * @param {number} id - id of media
	 * @param {string} slug - slug of media
	 * @returns {Object} array of medias
	 */
	getMedia({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Media.findOne({ where });
	}
}
