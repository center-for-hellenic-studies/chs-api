import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Role from '../../models/role';


/**
 * Logic-layer service for dealing with roles
 */

export default class RoleService extends PermissionsService {
	/**
	 * Count roles
	 * @returns {number} count of roles
	 */
	count() {
		return Role.count();
	}

	/**
	 * Get a list of roles
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of roles
	 */
	getRoles({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Role.findAll(args);
	}

	/**
	 * Get role
	 * @param {number} id - id of role
	 * @param {string} slug - slug of role
	 * @returns {Object} array of roles
	 */
	getRole({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Role.findOne({ where });
	}
}
