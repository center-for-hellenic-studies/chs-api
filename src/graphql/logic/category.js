import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import Category from '../../models/category';


/**
 * Logic-layer service for dealing with categories
 */

export default class CategoryService extends PermissionsService {
	/**
	 * Count categories
	 * @returns {number} count of categories
	 */
	count() {
		return Category.count();
	}

	/**
	 * Get a list of categories
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of categories
	 */
	getCategories({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return Category.findAll(args);
	}

	/**
	 * Get category
	 * @param {number} id - id of category
	 * @param {string} slug - slug of category
	 * @returns {Object} array of categories
	 */
	getCategory({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return Category.findOne({ where });
	}
}
