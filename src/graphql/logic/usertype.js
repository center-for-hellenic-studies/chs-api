import Sequelize from 'sequelize';

import PermissionsService from './PermissionsService';
import UserType from '../../models/usertype';


/**
 * Logic-layer service for dealing with usertypes
 */

export default class UserTypeService extends PermissionsService {
	/**
	 * Count usertypes
	 * @returns {number} count of usertypes
	 */
	count() {
		return UserType.count();
	}

	/**
	 * Get a list of usertypes
	 * @param {number} limit
	 * @param {number} offset
	 * @param {string} textsearch
	 * @returns {Object[]} array of usertypes
	 */
	getUserTypes({ limit = 100, offset = 0, textsearch = null }) {
		const args = {
			where: {},
			limit,
			offset,
			order: [
				['id', 'ASC']
			],
		};


		if (textsearch) {
			args.where.title = {
				[Sequelize.Op.like]: `%${textsearch}%`,
			};
		}

		return UserType.findAll(args);
	}

	/**
	 * Get usertype
	 * @param {number} id - id of usertype
	 * @param {string} slug - slug of usertype
	 * @returns {Object} array of usertypes
	 */
	getUserType({ id, slug }) {
		const where = {};

		if (!id && !slug) {
			return null;
		}

		if (id) {
			where.id = id;
		}

		if (slug) {
			where.slug = slug;
		}

		return UserType.findOne({ where });
	}
}
