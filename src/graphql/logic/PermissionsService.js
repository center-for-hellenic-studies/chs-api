import jsonwebtoken from 'jsonwebtoken';

import User from '../../models/user';

/**
 * The PermissionsService supports only 'admin' role currently
 */
export default class PermissionsService {

	constructor(token) {
		this.userId = null;
		this.userName = null;
		this.userAvatar = null;
		this.project = null;

		if (token !== 'null' && token) {
			this.token = token;
			const decoded = jsonwebtoken.decode(token);
			if (decoded) {
				this.userId = decoded.userId;
				this.userName = decoded.userName;
				this.userAvatar = decoded.userAvatar;
			}
		}
	}
}
