<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Class: exports</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Class: exports</h1>

    




<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with items</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_item.js.html">graphql/logic/item.js</a>, <a href="graphql_logic_item.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with sites</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_site.js.html">graphql/logic/site.js</a>, <a href="graphql_logic_site.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with menus</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_menu.js.html">graphql/logic/menu.js</a>, <a href="graphql_logic_menu.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with topics</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_topic.js.html">graphql/logic/topic.js</a>, <a href="graphql_logic_topic.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with collections</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_collection.js.html">graphql/logic/collection.js</a>, <a href="graphql_logic_collection.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with languages</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_language.js.html">graphql/logic/language.js</a>, <a href="graphql_logic_language.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">The PermissionsService supports only 'admin' role currently</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_PermissionsService.js.html">graphql/logic/PermissionsService.js</a>, <a href="graphql_logic_PermissionsService.js.html#line8">line 8</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with users</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_user.js.html">graphql/logic/user.js</a>, <a href="graphql_logic_user.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with roles</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_role.js.html">graphql/logic/role.js</a>, <a href="graphql_logic_role.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with categories</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_category.js.html">graphql/logic/category.js</a>, <a href="graphql_logic_category.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with articles</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_article.js.html">graphql/logic/article.js</a>, <a href="graphql_logic_article.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with usertypes</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_usertype.js.html">graphql/logic/usertype.js</a>, <a href="graphql_logic_usertype.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>







<section>

<header>
    
        <h2><span class="attribs"><span class="type-signature"></span></span>exports<span class="signature">()</span><span class="type-signature"></span></h2>
        
            <div class="class-description">Logic-layer service for dealing with medias</div>
        
    
</header>

<article>
    <div class="container-overview">
    
        

    
    <h2>Constructor</h2>
    

    
    <h4 class="name" id=".exports"><span class="type-signature"></span>new exports<span class="signature">()</span><span class="type-signature"></span></h4>
    

    















<dl class="details">

    

    

    

    

    

    

    

    

    

    

    

    

    
    <dt class="tag-source">Source:</dt>
    <dd class="tag-source"><ul class="dummy"><li>
        <a href="graphql_logic_media.js.html">graphql/logic/media.js</a>, <a href="graphql_logic_media.js.html#line11">line 11</a>
    </li></ul></dd>
    

    

    

    
</dl>


















    
    </div>

    

    

    

    

    

    

    

    

    

    
</article>

</section>




</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Classes</h3><ul><li><a href="EmailManager.html">EmailManager</a></li><li><a href="module.html#.exports">exports</a></li></ul><h3>Global</h3><ul><li><a href="global.html#ArgumentError">ArgumentError</a></li><li><a href="global.html#Article">Article</a></li><li><a href="global.html#ArticleInputType">ArticleInputType</a></li><li><a href="global.html#ArticleType">ArticleType</a></li><li><a href="global.html#AuthenticationError">AuthenticationError</a></li><li><a href="global.html#Category">Category</a></li><li><a href="global.html#CategoryType">CategoryType</a></li><li><a href="global.html#Collection">Collection</a></li><li><a href="global.html#CollectionType">CollectionType</a></li><li><a href="global.html#count">count</a></li><li><a href="global.html#createRemoteSchema">createRemoteSchema</a></li><li><a href="global.html#db">db</a></li><li><a href="global.html#generateJWT">generateJWT</a></li><li><a href="global.html#getArticle">getArticle</a></li><li><a href="global.html#getArticles">getArticles</a></li><li><a href="global.html#getCategories">getCategories</a></li><li><a href="global.html#getCategory">getCategory</a></li><li><a href="global.html#getCollection">getCollection</a></li><li><a href="global.html#getCollections">getCollections</a></li><li><a href="global.html#getItem">getItem</a></li><li><a href="global.html#getItems">getItems</a></li><li><a href="global.html#getLanguage">getLanguage</a></li><li><a href="global.html#getLanguages">getLanguages</a></li><li><a href="global.html#getMedia">getMedia</a></li><li><a href="global.html#getMedias">getMedias</a></li><li><a href="global.html#getMenu">getMenu</a></li><li><a href="global.html#getMenus">getMenus</a></li><li><a href="global.html#getRole">getRole</a></li><li><a href="global.html#getRoles">getRoles</a></li><li><a href="global.html#getSite">getSite</a></li><li><a href="global.html#getSites">getSites</a></li><li><a href="global.html#getTopic">getTopic</a></li><li><a href="global.html#getTopics">getTopics</a></li><li><a href="global.html#getUser">getUser</a></li><li><a href="global.html#getUsers">getUsers</a></li><li><a href="global.html#getUserType">getUserType</a></li><li><a href="global.html#getUserTypes">getUserTypes</a></li><li><a href="global.html#handleMongooseError">handleMongooseError</a></li><li><a href="global.html#Item">Item</a></li><li><a href="global.html#ItemType">ItemType</a></li><li><a href="global.html#jwtAuthenticate">jwtAuthenticate</a></li><li><a href="global.html#Language">Language</a></li><li><a href="global.html#LanguageType">LanguageType</a></li><li><a href="global.html#loginOAuth1">loginOAuth1</a></li><li><a href="global.html#loginOAuth2">loginOAuth2</a></li><li><a href="global.html#loginPWD">loginPWD</a></li><li><a href="global.html#Media">Media</a></li><li><a href="global.html#MediaType">MediaType</a></li><li><a href="global.html#Menu">Menu</a></li><li><a href="global.html#MenuType">MenuType</a></li><li><a href="global.html#MongooseDuplicateKeyError">MongooseDuplicateKeyError</a></li><li><a href="global.html#MongooseGeneralError">MongooseGeneralError</a></li><li><a href="global.html#MongooseValidationError">MongooseValidationError</a></li><li><a href="global.html#oauthSetup">oauthSetup</a></li><li><a href="global.html#PermissionError">PermissionError</a></li><li><a href="global.html#ProjectError">ProjectError</a></li><li><a href="global.html#redisSetup">redisSetup</a></li><li><a href="global.html#RemoveType">RemoveType</a></li><li><a href="global.html#Role">Role</a></li><li><a href="global.html#RoleType">RoleType</a></li><li><a href="global.html#RootMutations">RootMutations</a></li><li><a href="global.html#RootQuery">RootQuery</a></li><li><a href="global.html#RootSchema">RootSchema</a></li><li><a href="global.html#setupGraphql">setupGraphql</a></li><li><a href="global.html#Site">Site</a></li><li><a href="global.html#SiteType">SiteType</a></li><li><a href="global.html#Topic">Topic</a></li><li><a href="global.html#TopicType">TopicType</a></li><li><a href="global.html#User">User</a></li><li><a href="global.html#UserType">UserType</a></li><li><a href="global.html#UserTypeType">UserTypeType</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.5.5</a> on Sat Feb 24 2018 20:13:49 GMT-0500 (EST)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>